const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  try {
    const token = req.body.headers.Authorization.split(' ')[1];
    const decodedToken = jwt.verify(token, 'NJKJBBHBD6655@BVHJVHJS78J.,[]NKJNBS%$#72656778816T63VHJSJNBJWYU');
    const userId = decodedToken.userId;
    if (req.body.userId && req.body.userId !== userId) {
      console.log('auth non ok')
      throw 'Invalid user ID';
    } else {
      console.log('auth ok')
      next();
    }
  } catch {
    res.status(401).json({
      error: new Error('Invalid request!')
    });
  }
};