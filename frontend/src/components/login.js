
import React, { Component } from 'react'
import axios from 'axios'

export default class Login extends Component {

    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            err: undefined
        }
        this.change = this.change.bind(this)
        this.submit = this.submit.bind(this)
    }

    change = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
        
    }

    submit (e) {
        e.preventDefault();
        const user ={
            email: this.state.email,
            password: this.state.password
        }
        axios.post('http://localhost:3000/api/auth/login', user)
        .then(res => {localStorage.setItem('jwt_token', res.data.token); console.log(res.data)})
    }

    render() {
        return (
            <div>
                <form onSubmit={e => this.submit(e)}>
                    <label>email</label><input type='email' name='email' value={this.state.email} onChange={e=>this.change(e)}/>
                    <label>password</label><input type='password' name='password' value={this.state.password} onChange={e=>this.change(e)}/>
                    <button type='submit'>login</button>
                </form>
            </div>
        )
    }
}
