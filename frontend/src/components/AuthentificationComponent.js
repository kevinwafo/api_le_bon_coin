import React, { Component } from 'react'
import axios from 'axios'
import {getJwt} from '../helper/jwt'

export default class AuthentificationComponent extends Component {

    constructor(props){
        super(props);
        this.state = {
            user: undefined
        }
    }

    componentDidMount(){
        const jwt = getJwt();
        if(!jwt){
            this.props.history.push('/login')
        }
        const userId = "5e7a36e87e34f161988dac88"
        axios.post('http://localhost:3000/api/auth/getUser',{userId, headers:{Authorization: `Bearer ${jwt}`} })
        .then(res => console.log(res.data))
        .catch(err => console.log(err))

    }

    render() {
        return (
            <div>
                <h1>Athentification</h1>
            </div>
        )
    }
}
