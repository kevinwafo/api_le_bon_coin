import React from 'react';

import {BrowserRouter,Switch,Route} from 'react-router-dom'

import Home from './components/Home'
import Login from './components/login'
import Header from './components/Header'

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Header/>
        <Route path='/login' component={Login} />
        <Route path='/' exact component={Home} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
